#ifndef HACKATHONAPP_H
#define HACKATHONAPP_H

#include "MooseApp.h"

class HackathonApp;

template<>
InputParameters validParams<HackathonApp>();

class HackathonApp : public MooseApp
{
public:
  HackathonApp(InputParameters parameters);
  virtual ~HackathonApp();

  static void registerApps();
  static void registerObjects(Factory & factory);
  static void associateSyntax(Syntax & syntax, ActionFactory & action_factory);
};

#endif /* HACKATHONAPP_H */
