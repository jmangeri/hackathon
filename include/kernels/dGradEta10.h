#ifndef DGRADETA10_H
#define DGRADETA10_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta10;

template<>
InputParameters validParams<dGradEta10>();

class dGradEta10: public Kernel
{
public:

  dGradEta10(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta10;
  const Real _kappa;


};
#endif //DFDETA10_H
