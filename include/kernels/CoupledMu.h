#ifndef COUPLEDMU_H
#define COUPLEDMU_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class CoupledMu;

template<>
InputParameters validParams<CoupledMu>();

class CoupledMu: public Kernel
{
public:

  CoupledMu(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const unsigned int _mu_var;
  const VariableValue & _mu;

};
#endif //COUPLEDMU_H
