#ifndef DGRADETA4_H
#define DGRADETA4_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta4;

template<>
InputParameters validParams<dGradEta4>();

class dGradEta4: public Kernel
{
public:

  dGradEta4(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  const VariableGradient & _grad_eta4;
  const Real _kappa;


};
#endif //DFDETA8_H
