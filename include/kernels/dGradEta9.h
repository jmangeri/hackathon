#ifndef DGRADETA9_H
#define DGRADETA9_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta9;

template<>
InputParameters validParams<dGradEta9>();

class dGradEta9: public Kernel
{
public:

  dGradEta9(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta9;
  const Real _kappa;


};
#endif //DFDETA9_H
