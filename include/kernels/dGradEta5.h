#ifndef DGRADETA5_H
#define DGRADETA5_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta5;

template<>
InputParameters validParams<dGradEta5>();

class dGradEta5: public Kernel
{
public:

  dGradEta5(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta5;
  const Real _kappa;


};
#endif //DFDETA5_H
