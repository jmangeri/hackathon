#ifndef DMUDPSI_H
#define DMUDPSI_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dMudPsi;

template<>
InputParameters validParams<dMudPsi>();

class dMudPsi: public Kernel
{
public:

  dMudPsi(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  const VariableValue & _mu;
  const VariableGradient & _mu_grad;
  const Real _D;

};
#endif //DMUDPSI_H
