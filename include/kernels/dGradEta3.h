#ifndef DGRADETA3_H
#define DGRADETA3_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta3;

template<>
InputParameters validParams<dGradEta3>();

class dGradEta3: public Kernel
{
public:

  dGradEta3(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta3;
  const Real _kappa;


};
#endif //DFDETA3_H
