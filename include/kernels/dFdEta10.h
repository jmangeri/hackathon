#ifndef DFDETA10_H
#define DFDETA10_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dFdEta10;

template<>
InputParameters validParams<dFdEta10>();

class dFdEta10: public Kernel
{
public:

  dFdEta10(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const unsigned int _c_var;
  const unsigned int _eta1_var;
  const unsigned int _eta2_var;
  const unsigned int _eta3_var;
  const unsigned int _eta4_var;
  const unsigned int _eta5_var;
  const unsigned int _eta6_var;
  const unsigned int _eta7_var;
  const unsigned int _eta8_var;
  const unsigned int _eta9_var;

  const VariableValue & _c;
  const VariableValue & _eta1;
  const VariableValue & _eta2;
  const VariableValue & _eta3;
  const VariableValue & _eta4;
  const VariableValue & _eta5;
  const VariableValue & _eta6;
  const VariableValue & _eta7;
  const VariableValue & _eta8;
  const VariableValue & _eta9;
  const VariableValue & _eta10;

  const Real _A;
  const Real _B;
  const Real _ca;
  const Real _cb;
  const Real _gamma;
  const Real _epsilon;
  const Real _beta;
  const Real _cm;
};
#endif //DFDETA1_H
