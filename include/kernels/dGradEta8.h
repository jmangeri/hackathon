#ifndef DGRADETA8_H
#define DGRADETA8_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta8;

template<>
InputParameters validParams<dGradEta8>();

class dGradEta8: public Kernel
{
public:

  dGradEta8(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  const VariableGradient & _grad_eta8;
  const Real _kappa;


};
#endif //DFDETA8_H
