#ifndef DFDC_H
#define DFDC_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class Dfdc;

template<>
InputParameters validParams<Dfdc>();

class Dfdc: public Kernel
{
public:

  Dfdc(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  const unsigned int _c_var;
  const VariableValue & _c;
  const Real _A;
  const Real _B;
  const Real _ca;
  const Real _cb;

};
#endif //Dfdc_H
