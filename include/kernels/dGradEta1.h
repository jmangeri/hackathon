#ifndef DGRADETA1_H
#define DGRADETA1_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta1;

template<>
InputParameters validParams<dGradEta1>();

class dGradEta1: public Kernel
{
public:

  dGradEta1(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta1;
  const Real _kappa;


};
#endif //DFDETA8_H
