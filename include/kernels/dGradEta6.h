#ifndef DGRADETA6_H
#define DGRADETA6_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta6;

template<>
InputParameters validParams<dGradEta6>();

class dGradEta6: public Kernel
{
public:

  dGradEta6(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta6;
  const Real _kappa;


};
#endif //DFDETA6_H
