#ifndef DGRADETA2_H
#define DGRADETA2_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta2;

template<>
InputParameters validParams<dGradEta2>();

class dGradEta2: public Kernel
{
public:

  dGradEta2(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:

  const VariableGradient & _grad_eta2;
  const Real _kappa;


};
#endif //DFDETA8_H
