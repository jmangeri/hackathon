#ifndef DGRADETA7_H
#define DGRADETA7_H

#include "Kernel.h"

// #include "Material.h"
// #include "DerivativeMaterialInterface.h"

//Forward Declarations
class dGradEta7;

template<>
InputParameters validParams<dGradEta7>();

class dGradEta7: public Kernel
{
public:

  dGradEta7(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();


private:

  const VariableGradient & _grad_eta7;
  const Real _kappa;


};
#endif //DFDETA7_H
