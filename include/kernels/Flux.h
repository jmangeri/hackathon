
#ifndef FLUX_H
#define FLUX_H
#include "FerretBase.h"
#include "Kernel.h"
#include "FEProblem.h"
#include "IntegratedBC.h"

//Forward Declarations
class Flux;

template<>
InputParameters validParams<Flux>();

class Flux : public IntegratedBC
{
public:

  /**
   * Factory constructor, takes parameters so that all derived classes can be built using the same
   * constructor.
   */
  Flux(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual();
  const MooseArray<Point> & _normals;

  private:
    const unsigned int _c_var;
    const VariableGradient & _c_grad;
    const VariableValue & _c;
    VariableThird & _third_c;
    VariableTestSecond & _second_test;
    VariablePhiSecond & _second_phi;
    const Real _kappa;
    const Real _D;
};

#endif
