/**
 */

#ifndef FREEENERGY_H
#define FREEENERGY_H

#include "AuxKernel.h"
#include "RankTwoTensor.h"
#include "ElementIntegralPostprocessor.h"

//Forward Declarations
class FreeEnergy;

template<>
InputParameters validParams<FreeEnergy>();


class FreeEnergy : public ElementIntegralPostprocessor
{
public:
  FreeEnergy(const InputParameters & parameters);

protected:
  virtual Real computeQpIntegral();

private:
  const VariableGradient & _c_grad;
  const VariableValue & _c;
  const Real _A;
  const Real _B;
  const Real _ca;
  const Real _cb;
  const Real _D;
};

#endif
