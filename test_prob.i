[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 180
  ny = 180
  xmin = 0
  xmax = 150
  ymin = 0
  ymax = 150
  elem_type = QUAD4
[]

[Functions]
  [./ic_func1]
    type = ParsedFunction
    value = '0.45 + alpha * cos(0.141421 * x + 0.173205 * y)'
    vars = 'alpha'
    vals = '0.01'
  [../]
  #eta q_i = <\sqrt{23+i},\sqrt{129 +i}>
  #{4.89898, 5., 5.09902, 5.19615, 5.2915, 5.38516, 5.47723, 5.56776, 5.65685, 5.74456}
  #{12.2474, 12.2882, 12.3288, 12.3693, 12.4097, 12.4499, 12.49, 12.53, 12.5698, 12.6095}

  [./ic_eta1_func]
    type = ParsedFunction
    value = 'zeta * cos(4.89898 * x + 12.2474 * y) * cos(4.89898 * x + 12.2474 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta2_func]
    type = ParsedFunction
    value = 'zeta * cos(5 * x + 12.2882 * y) * cos(5 * x + 12.2882 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta3_func]
    type = ParsedFunction
    value = 'zeta * cos(5.099 * x + 12.3288 * y) * cos(5.099 * x + 12.3288 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta4_func]
    type = ParsedFunction
    value = 'zeta * cos(5.19615 * x + 12.3693 * y) * cos(5.19615 * x + 12.3693 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta5_func]
    type = ParsedFunction
    value = 'zeta * cos(5.2915 * x + 12.4097 * y) * cos(5.2915 * x + 12.4097 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta6_func]
    type = ParsedFunction
    value = 'zeta * cos(5.38516 * x + 12.4499 * y) * cos(5.38516 * x + 12.4499 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta7_func]
    type = ParsedFunction
    value = 'zeta * cos(5.47723 * x + 12.49 * y) * cos(5.47723 * x + 12.49 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta8_func]
    type = ParsedFunction
    value = 'zeta * cos(5.56776 * x + 12.53 * y) * cos(5.56776 * x + 12.53 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta9_func]
    type = ParsedFunction
    value = 'zeta * cos(5.65685 * x + 12.5698 * y) * cos(5.65685 * x + 12.5698 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]
  [./ic_eta10_func]
    type = ParsedFunction
    value = 'zeta * cos(5.74456 * x + 12.6095 * y) * cos(5.74456 * x + 12.6095 * y)'
    vars = 'zeta'
    vals = '0.001'
  [../]



  #[./ic_func2]
  #  type = ParsedFunction
  #  value = ' 0.05 * (0.45- 0.05 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 +
  #   0.95 * (0.45- 0.95 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 -
  #   2.0 * (0.45- 0.5 + 0.01 * cos(0.141421 * x + 0.173205 * y)) +
  #   9.87654 * (0.45- 0.5 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 +
  #   0.0005 * 2.0 * cos(0.141421 * x + 0.173205 * y)'
  #[../]
[]

[GlobalParams]
  eta1 = eta1
  eta2 = eta2
  eta3 = eta3
  eta4 = eta4
  eta5 = eta5
  eta6 = eta6
  eta7 = eta7
  eta8 = eta8
  eta9 = eta9
  eta10 = eta10
  c = c
  mu = mu
  kappa = 2.0
  beta = 1.0
  epsilon = 3.0
  gamma = 2.46914
  ca = 0.05
  cb = 0.95
  B = 9.87654
  cm = 0.5
[]


[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_func1
    [../]
  [../]
  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta1_func
    [../]
  [../]
  [./eta2]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta2_func
    [../]
  [../]
  [./eta3]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta3_func
    [../]
  [../]
  [./eta4]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta4_func
    [../]
  [../]
  [./eta5]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta5_func
    [../]
  [../]
  [./eta6]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta6_func
    [../]
  [../]
  [./eta7]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta7_func
    [../]
  [../]
  [./eta8]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta8_func
    [../]
  [../]
  [./eta9]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta9_func
    [../]
  [../]
  [./eta10]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = ic_eta10_func
    [../]
  [../]
[]

[Kernels]
  [./coupledmu]
    type = CoupledMu
    variable = c
    mu = mu
    c = c
  [../]
  [./Lc]
    type = Lc
    variable = c
    kappa = 2.0
    c = c
    mu = mu
  [../]
  #[./Dfdc]
  #  type = Dfdc
  #  variable = c
  #  mu = mu
  #  c = c
  #  A = 2.0
  #  ca = 0.05
  #  cb = 0.95
  #  B = 9.87654
  #[../]
  [./Dmudpsi]
    type = dMudPsi
    variable = mu
    D = 2.46914 #note \gamma is D now in problem 2
    mu = mu
    c = c
  [../]

  [./Df2dc]
    type = Df2dc
    variable = c
    mu = mu
    c = c
    A = 2.0
    gamma = 2.46914
    epsilon = 3.0
    beta = 1.0
    ca = 0.05
    cb = 0.95
    B = 9.87654
  [../]

  [./dGradeta1]
    type = dGradEta1
    variable = eta1
  [../]
  [./dGradeta2]
    type = dGradEta2
    variable = eta2
  [../]
  [./dGradeta3]
    type = dGradEta3
    variable = eta3
  [../]
  [./dGradeta4]
    type = dGradEta4
    variable = eta4
  [../]
  [./dGradeta5]
    type = dGradEta5
    variable = eta5
  [../]
  [./dGradeta6]
    type = dGradEta6
    variable = eta6
  [../]
  [./dGradeta7]
    type = dGradEta7
    variable = eta7
  [../]
  [./dGradeta8]
    type = dGradEta8
    variable = eta8
  [../]
  [./dGradeta9]
    type = dGradEta9
    variable = eta9
  [../]
  [./dGradeta10]
    type = dGradEta10
    variable = eta10
  [../]



  [./dfdeta1]
    type = dFdEta1
    variable = eta1
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta2]
    type = dFdEta2
    variable = eta2
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta3]
    type = dFdEta3
    variable = eta3
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta4]
    type = dFdEta4
    variable = eta4
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta5]
    type = dFdEta5
    variable = eta5
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta6]
    type = dFdEta6
    variable = eta6
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta7]
    type = dFdEta7
    variable = eta7
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta8]
    type = dFdEta8
    variable = eta8
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta9]
    type = dFdEta9
    variable = eta9
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]
  [./dfdeta10]
    type = dFdEta10
    variable = eta10
    gamma = 2.46914
    beta = 1.0
    epsilon = 3.0
  [../]


  [./ctime]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]
  [./eta1_time]
    type = TimeDerivative
    variable = eta1
  [../]
  [./eta2_time]
    type = TimeDerivative
    variable = eta2
  [../]
  [./eta3_time]
    type = TimeDerivative
    variable = eta3
  [../]
  [./eta4_time]
    type = TimeDerivative
    variable = eta4
  [../]
  [./eta5_time]
    type = TimeDerivative
    variable = eta5
  [../]
  [./eta6_time]
    type = TimeDerivative
    variable = eta6
  [../]
  [./eta7_time]
    type = TimeDerivative
    variable = eta7
  [../]
  [./eta8_time]
    type = TimeDerivative
    variable = eta8
  [../]
  [./eta9_time]
    type = TimeDerivative
    variable = eta9
  [../]
  [./eta10_time]
    type = TimeDerivative
    variable = eta10
  [../]
[]

[BCs]
  #[./Periodic]
  #  [./c1]
  #     variable = c
  #     auto_direction = 'x y'
  #  [../]
  #[../]
  #[./x1]
  #  type = NeumannBC
  #  variable = c
  #  value = 0
  #  boundary = 'right'
  #[../]
  #[./x2]
  #  type = NeumannBC
  #  variable = c
  #  value = 0
  #  boundary = 'left'
  #[../]
  #[./y1]
  #  type = NeumannBC
  #  variable = c
  #  value = 0
  #  boundary = 'top'
  #[../]
  #[./y2]
  #  type = NeumannBC
  #  variable = c
  #  value = 0
  #  boundary = 'bottom'
  #[../]
[]

[Postprocessors]
  [./free]
    type = FreeEnergy
    c = c
    A = 2.0
    ca = 0.05
    cb = 0.95
    B = 9.87654
  [../]
  [./perc_change]
    type = PercentChangePostprocessor
    postprocessor = free
  [../]
  [./|R(i)|]
    type = Residual
  [../]
[]

#[UserObjects]
#  [./kill]
#    type = Terminator
#    expression = 'perc_change <= 1.0e-5'
#  [../]
#[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
    petsc_options_iname = '-ksp_gmres_restart  -snes_rtol -ksp_rtol -pc_type   -pc_asm_overlap -sub_pc_type -sub_pc_factor_zeropivot -pc_factor_zeropivot -pc_side '
    petsc_options_value = '    250          1e-3    1e-12         asm       10          ilu    1e-50    1e-50      left        '
  [../]
[]

#-snes_ksp_ew
[Executioner]
  type = Transient
  #[./TimeStepper] #iterative DT halfs the time it takes to find a solution? oh well, our time is fake in this simulation anyway...
  #  type = IterationAdaptiveDT
  #  dt = 0.000005 #max seems to be about 1.0 but could depend on refinement...
  #  #there is also a cutback on this for 0.2*optimal and yes i think it does count the 0th one.
  #  #iteration_window = 10
  #  optimal_iterations = 15 #i think this is 3 or more then cut? less than 3 grow, does it count the 0th iteration? no the cutting has to do with the iteration ratio
  #  growth_factor = 1.4
  #  linear_iteration_ratio = 100
  #  #linear_iteration_ratio = 1000
  #  cutback_factor =  0.8
  #[../]
  solve_type = 'NEWTON'       #"PJFNK, JFNK, NEWTON"
  scheme = 'bdf2'   #"implicit-euler, explicit-euler, crank-nicolson, bdf2, rk-2"
  dt = 0.1
  dtmin = 1e-13
  dtmax = 8.2
  num_steps = 1000000
[]

[Outputs]
  print_linear_residuals = true
  print_perf_log = true
  [./out]
    type = Exodus
    file_base = out_spinodal_test_neumann_prob
    output_initial = true
    elemental_as_nodal = true
    interval = 1
  [../]
[]
