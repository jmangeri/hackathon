/**
 */

#include "dGradEta6.h"

template<>
InputParameters validParams<dGradEta6>()
{
  InputParameters params = validParams<Kernel>();
  params.addRequiredCoupledVar("c", "concentration");
  params.addRequiredCoupledVar("eta6", "phase6");
  params.addParam<Real>("kappa", 1.0, "kappa");
  return params;
}

dGradEta6::dGradEta6(const InputParameters & parameters)
  :Kernel(parameters),
   _grad_eta6(coupledGradient("eta6")),
   _kappa(getParam<Real>("kappa"))
{
}

Real
dGradEta6::computeQpResidual()
{
  return  _kappa * _grad_eta6[_qp] * _grad_test[_i][_qp];
}

Real
dGradEta6::computeQpJacobian()
{
  return  _kappa * _grad_phi[_j][_qp] * _grad_test[_i][_qp];
}
