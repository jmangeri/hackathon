[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 200
  ny = 200
  xmin = 0
  xmax = 200
  ymin = 0
  ymax = 200
  elem_type = QUAD8
[]

[Functions]
  [./ic_func1]
    type = ParsedFunction
    value = '0.45 + alpha * cos(0.141421 * x + 0.173205 * y)'
    vars = 'alpha'
    vals = '0.01'
  [../]
  #[./ic_func2]
  #  type = ParsedFunction
  #  value = ' 0.05 * (0.45- 0.05 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 +
  #   0.95 * (0.45- 0.95 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 -
  #   2.0 * (0.45- 0.5 + 0.01 * cos(0.141421 * x + 0.173205 * y)) +
  #   9.87654 * (0.45- 0.5 + 0.01 * cos(0.141421 * x + 0.173205 * y))^3 +
  #   0.0005 * 2.0 * cos(0.141421 * x + 0.173205 * y)'
  #[../]
[]

[Variables]
  [./c]
    order = SECOND
    family = MONOMIAL
    block = '0'
    [./InitialCondition]
      type = FunctionIC
      function = ic_func1
    [../]
  [../]
  #
  #[./mu]
  #  order = FIRST
  #  family = LAGRANGE
  #  block = '0'
  #  #[./InitialCondition]
  #  #  type = FunctionIC
  #  #  function = ic_func2
  #  #[../]
  #[../]
[]

[Kernels]
  #[./coupledmu]
  #  type = CoupledMu
  #  variable = c
  #  mu = mu
  #  c = c
  #[../]
  #[./Lc]
  #  type = Lc
  #  variable = c
  #  kappa = 2.0
  #  c = c
  #  mu = mu
  #[../]
  #[./Dfdc]
  #  type = Dfdc
  #  variable = c
  #  mu = mu
  #  c = c
  #  A = 2.0
  #  ca = 0.05
  #  cb = 0.95
  #  B = 9.87654
  #[../]
  #[./Dmudpsi]
  #  type = dMudPsi
  #  variable = mu
  #  D = 2.22222
  #  mu = mu
  #  c = c
  #[../]
  [./fourthderivative]
    type = Fourth
    variable = c
    c = c
    kappa = 2.0
    D = 2.22222
  [../]
  [./free_energy]
    type = Free
    variable = c
    c = c
    A = 2.0
    B = 9.87654
    D = 2.22222
  [../]


  [./ctime]
    type = TimeDerivative
    variable = c

  [../]
[]

[BCs]
  [./Periodic]
    [./c1]
       variable = c
       auto_direction = 'x y'
    [../]
  [../]
#  [./x1]
#    type = NeumannBC
#    variable = c
#    value = 0
#    boundary = 'right'
#  [../]
#  [./x2]
#    type = NeumannBC
#    variable = c
#    value = 0
#    boundary = 'left'
#  [../]
#  [./y1]
#    type = NeumannBC
#    variable = c
#    value = 0
#    boundary = 'top'
#  [../]
#  [./y2]
#    type = NeumannBC
#    variable = c
#    value = 0
#    boundary = 'bottom'
#  [../]
#[]

[Postprocessors]
  [./free]
    type = FreeEnergy
    c = c
    A = 2.0
    ca = 0.05
    cb = 0.95
    B = 9.87654
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
    petsc_options = '-snes_view -snes_linesearch_monitor -snes_converged_reason -ksp_converged_reason -options_left '
    petsc_options_iname = '-ksp_gmres_restart  -snes_rtol -ksp_rtol -pc_type   -pc_asm_overlap -sub_pc_type -sub_pc_factor_zeropivot -pc_factor_zeropivot -pc_side '
    petsc_options_value = '    651           1e-8      1e-8          asm          10          ilu    1e-50    1e-50      left        '
  [../]
[]

#-snes_ksp_ew
[Executioner]
  type = Transient
  #[./TimeStepper] #iterative DT halfs the time it takes to find a solution? oh well, our time is fake in this simulation anyway...
  #  type = IterationAdaptiveDT
  #  dt = 0.000005 #max seems to be about 1.0 but could depend on refinement...
  #  #there is also a cutback on this for 0.2*optimal and yes i think it does count the 0th one.
  #  #iteration_window = 10
  #  optimal_iterations = 15 #i think this is 3 or more then cut? less than 3 grow, does it count the 0th iteration? no the cutting has to do with the iteration ratio
  #  growth_factor = 1.4
  #  linear_iteration_ratio = 100
  #  #linear_iteration_ratio = 1000
  #  cutback_factor =  0.8
  #[../]
  solve_type = 'PJFNK'       #"PJFNK, JFNK, NEWTON"
  scheme = 'bdf2'   #"implicit-euler, explicit-euler, crank-nicolson, bdf2, rk-2"
  dt = 0.5
  dtmin = 1e-13
  dtmax = 8.2
  num_steps = 100000
[]

[Outputs]
  print_linear_residuals = true
  print_perf_log = true
  [./out]
    type = Exodus
    file_base = out_spinodal_test
    output_initial = true
    elemental_as_nodal = true
    interval = 1
  [../]
[]
